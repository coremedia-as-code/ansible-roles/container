# python 3 headers, required if submitting to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

# from ansible.errors import AnsibleError, AnsibleParserError
# from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display

import json

# https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html
# https://blog.oddbit.com/post/2019-04-25-writing-ansible-filter-plugins/

display = Display()


class FilterModule(object):
    ''' Ansible file jinja2 tests '''

    def filters(self):
        return {
            'container_hashes': self.filter_hashes,
            'compare_dict': self.filter_compare_dict,
            'container_names': self.filter_names,
            'container_images': self.filter_images,
            'remove_environments': self.filter_remove_env,
            'changed': self.filter_changed,
            'update': self.filter_update,
        }

    """

    """
    def filter_hashes(self, mydict):
        """
            return basic information about containers
        """
        seen = {}
        data = {}

        if(isinstance(mydict, list)):
            data = mydict['results']

        for i in data:
            if(isinstance(i, dict)):
                cont = {}
                item = {}

                if('container' in i):
                    cont = i.get('container')
                if('item' in i):
                    item = i.get('item')

                if(cont):
                    # for x in ['HostConfig', 'State', 'GraphDriver', 'NetworkSettings']:
                    #    _ = container.pop(x)
                    name = cont.get('Name').strip("/")
                    display.vv("found: {}".format(name))

                    image = cont.get('Config').get('Image')
                    created = cont.get('Created')
                elif(item):
                    name = item.get('name')
                    display.vv("found: {}".format(name))

                    image = item.get('image')
                    created = "None"
                else:
                    pass
            else:
                pass

            registry = image.split('/')[0]
            container = image.split('/')[1].split(':')[0]
            container_tag = image.split(':')[1]

            seen[name] = {
                "container": container,
                "registry": registry,
                "tag": container_tag,
                "created": created,
            }

        display.v("return : {}".format(seen))

        return seen

    def filter_compare_dict(self, left_dict, right_dict):
        result = {}

        if(isinstance(left_dict, list)):

            # display.v("rebuild left")
            _dict = {}

            for e in left_dict:
                # display.v("  {}".format(e))
                # display.v("  {}".format(e.get('name')))
                name = e.get('name')
                image = e.get('image')

                registry = image.split('/')[0]
                container = image.split('/')[1].split(':')[0]
                container_tag = image.split(':')[1]

                _dict[name] = {
                    "container": container,
                    "registry": registry,
                    "tag": container_tag,
                    "created": "None",
                }

            left_dict = _dict

        # display.vv("left")
        # display.vv(json.dumps(left_dict, indent=2))
        # display.vv("right")
        # display.vv(json.dumps(right_dict, indent=2))

        for k in left_dict:
            # display.v("  {}".format(k))

            l_dict = left_dict[k]
            r_dict = right_dict[k]
            _ = l_dict.pop('created')
            _ = r_dict.pop('created')

            if (k not in right_dict):
                result[k] = l_dict
            else:
                left = json.dumps(l_dict, sort_keys=True)
                right = json.dumps(r_dict, sort_keys=True)

                display.v(json.dumps(left, indent=2))
                display.v(json.dumps(right, indent=2))

                if(left != right):
                    result[k] = l_dict

        # display.v("return : {}".format(result))

        return result

    def filter_names(self, data):
        """

        """
        return self._get_keys_from_dict(data, 'name')

    def filter_images(self, data):
        """

        """
        return self._get_keys_from_dict(data, 'image')

    def filter_remove_env(self, data):
        """

        """
        return self._del_keys_from_dict(data, 'environments')

    def filter_changed(self, data):
        """

        """
        result = []

        if(isinstance(data, dict)):
            data = data['results']

        for i in data:
            # display.v( json.dumps( i, indent = 2 ) )

            # display.v("name   : {}".format(i.get('item')))
            # display.v("  -    : {}".format(i.get('image',{}).get('ContainerConfig',{}).get('Image',None) ))
            # display.v("  -    : {}".format(i.get('image',{}).get('Config',{}).get('Image',None) ))

            if(isinstance(i, dict)):
                changed = i.get('changed', False)
                item = i.get('item', None)
                display.vv("item   : {}".format(item))
                display.vv("changed: {}".format(changed))
                display.vv("actions: {}".format(i.get('actions', None)))

                if(changed):
                    result.append(item)

        return result

    def filter_update(self, data, update):
        """
          add recreate to changed container entry
        """
        for change in update:
            for d in data:
                if(d.get('image') == change):
                    d['recreate'] = "true"

        return data

    def diff_dicts(self, a, b, missing=KeyError):
        """
        Find keys and values which differ from `a` to `b` as a dict.

        If a value differs from `a` to `b` then the value in the returned dict will
        be: `(a_value, b_value)`. If either is missing then the token from
        `missing` will be used instead.

        :param a: The from dict
        :param b: The to dict
        :param missing: A token used to indicate the dict did not include this key
        :return: A dict of keys to tuples with the matching value from a and b
        """
        return {
            key: (a.get(key, missing), b.get(key, missing))
            for key in dict(
                set(a.items()) ^ set(b.items())
            ).keys()
        }

    def recursive_compare(self, d1, d2, level='root'):
        if isinstance(d1, dict) and isinstance(d2, dict):
            if d1.keys() != d2.keys():
                s1 = set(d1.keys())
                s2 = set(d2.keys())
                print('{:<20} + {} - {}'.format(level, s1-s2, s2-s1))
                common_keys = s1 & s2
            else:
                common_keys = set(d1.keys())

            for k in common_keys:
                self.recursive_compare(d1[k], d2[k], level='{}.{}'.format(level, k))

        elif isinstance(d1, list) and isinstance(d2, list):
            if len(d1) != len(d2):
                print('{:<20} len1={}; len2={}'.format(level, len(d1), len(d2)))
            common_len = min(len(d1), len(d2))

            for i in range(common_len):
                self.recursive_compare(d1[i], d2[i], level='{}[{}]'.format(level, i))

        else:
            if d1 != d2:
                print('{:<20} {} != {}'.format(level, d1, d2))

    def _get_keys_from_dict(self, dictionary, key):
        result = []
        for i in dictionary:
            if(isinstance(i, dict)):
                k = i.get(key, None)
                if(k):
                    display.v("=> {}".format(k))
                    result.append(k)

        return result

    def _del_keys_from_dict(self, dictionary, key):
        """

        """
        for i in dictionary:
            if(isinstance(i, dict)):
                _ = i.pop(key, None)

        return dictionary
